// Set Up the Dependencies
const express = require('express');
const mongoose = require('mongoose');

// Course Original Resource Sharing Setting
const cors = require('cors');

// Routes
const userRoutes = require('./routes/user')
const productRoutes = require('./routes/product')
const orderRoutes = require('./routes/order')

// Server
const app = express();
const port = 4000;

// Enable CORS
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/orders', orderRoutes) 


// DB Connection
mongoose.connect("mongodb+srv://adriannfurigay:qwerty123@wdc028-course-booking.umyxd.mongodb.net/ecommerce-api?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connection to the cloud database"))


app.listen(process.env.PORT || port, () => console.log(`Now listening to port ${process.env.PORT || port}`));