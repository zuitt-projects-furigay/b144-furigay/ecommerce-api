const Product = require('../models/Product');
const Order = require('../models/Order');
const User = require('../models/User')
const auth = require('../auth')

// ---------------------------------------

// Retrieve all Active Products

module.exports.getAllActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}


// ---------------------------------------

// Retrieve Single Product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// ---------------------------------------

// Create Product (Admin Only)

module.exports.addProduct = (reqBody, userData) =>{
	return Product.findById(userData.userId).then(result => {

		if(userData.isAdmin == false) {
			return "You are not an admin"
		}
		else {
			let newProduct = new Product ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})

			// Save the created object to the DB
			return newProduct.save().then((product, error) => {
				// Product creation failed
				if(error) {
					return false;
				}
				else {
				// Product creation successful
					return "Product creation successful" 
				}
			})
		}
	})
}


// ------------------------------------------

// Update Product Information (Admin Only)

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}


// ------------------------------------------

// Archive Product (Admin Only)

module.exports.archiveProduct = (reqParams) => {
	let updatedActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((product, error) => {

		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}