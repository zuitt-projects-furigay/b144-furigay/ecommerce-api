const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// ------------------------------------------

// User Registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	// Save
	return newUser.save().then((user, error) => {
		// Registration Failed
		if(error) {
			return false;
		} 
		else {
		// Registration Success
			return true;
		}
	})
}


// ---------------------------------------

// User Authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)


			if(isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result.toObject())}
			}
			else {
				return false;
			}

		}
	})
}


// --------------------------------------

// Set as Admin

module.exports.setAsAdmin = (reqParams, reqBody) => {
	let setAsAdministrator = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, setAsAdministrator).then((user, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}

// --------------------------------------------

// Non-admin User Checkout (Create Order)

module.exports.checkout = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.purchased.push({productId: data.productId})

		return user.save().then((user, error) => {
			if(error) {
				return false;
			}
			else {
				return true;
			}

		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.purchaser.push({userId: data.userId})

		return product.save().then((course, error) => {
			if(error) {
				return false;
			}
			else {
				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {
		return true;
	}
	else {
		return false;
	}
}


// -----------------------------------------------

// Retrieve Authenticated user's order

module.exports.myOrders = (reqParams) => {
	return User.findById(reqParams.userId, {password: 0, email: 0, isAdmin: 0, _id: 0}).then(result => {
			
		return result;
		
	})
}


//-------------------------------------------------

// Retrieving all order (Admin only)


module.exports.getAllOrders = () => {
	return User.find({}).then((result, error) => {

		if(error) {
			return false;
		}
		else {
			return result;
		}
		
	})
}