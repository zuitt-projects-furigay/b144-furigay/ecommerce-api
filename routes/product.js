const express = require('express');
const router = express.Router();
const auth = require('../auth');
const productController = require('../controllers/product')

// -------------------------------------

// Retrieve all Active Products

router.get("/", (req, res) => {
	productController.getAllActiveProduct().then(result => res.send(result))
})


// -------------------------------------

// Retrieve Single Product

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(result => res.send(result))
})


// -------------------------------------

// Create Product (Admin Only)

router.post("/addProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})


// -------------------------------------

// Update Product Information (Admin Only)

router.put("/:productId/updateProduct", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
})



// -------------------------------------

// Archive Product (Admin Only)

router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(result => res.send(result))
})



module.exports = router;