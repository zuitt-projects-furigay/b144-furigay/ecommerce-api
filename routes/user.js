const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/user')

// ------------------------------------------

//Routes for User Registration
 
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


// ------------------------------------------

// Routes for User Authentication

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})


// ------------------------------------------

// Route for Set as Admin

router.put('/:userId/setAsAdmin', (req, res) => {
	userController.setAsAdmin(req.params, req.body).then(result => res.send(result));
})

// ------------------------------------------

// Routes for Non-admin User Checkout (Create Order)

router.post("/checkout", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.checkout(data).then(result => res.send(result))
})



// -----------------------------------------------

//Routes for Retrieving Authenticated user's order

router.get("/:userId/myOrders", auth.verify, (req, res) => {
	console.log(req.params.userId)

	userController.myOrders(req.params).then(result => res.send(result))
})


//-------------------------------------------------

// Routes for Retrieving all order (Admin only)

// retireve all order (Admin Only)
router.get('/orders', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getAllOrders(req.body, {isAdmin: userData}).then(result => res.send(result));
});




module.exports = router;