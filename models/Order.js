const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema ({

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	orders: [
		{
			userId: {
				type: String
				// required: [true, "User ID is required"]
			},

			productId: {
				type: String
				// required: [true, "Product ID is required"]
			}
		}
	]

})

module.exports = mongoose.model('Order', orderSchema)